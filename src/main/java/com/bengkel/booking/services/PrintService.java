package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.ItemServiceRepository;
import com.bengkel.booking.repositories.list_order.ListBookingOrder;
import com.bengkel.booking.services.generate.GenerateBookingOrderId;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.

	public static void printInformationCustomer(Customer customer) {
		String [] menuInformationCustomer = {"Customer Id", "Nama", "Customer Status", "Alamat", "Saldo Koin"};
		String [] informationCustomer = {customer.getCustomerId(), customer.getName(), (customer instanceof MemberCustomer) ? "Member" : "Non Member", customer.getAddress(), (customer instanceof MemberCustomer) ? Double.toString(((MemberCustomer)customer).getSaldoCoin()) : ""};
		
		for (int i = 0; i < informationCustomer.length; i++) {
			if (!(customer instanceof MemberCustomer) && i == informationCustomer.length - 1) continue;
			System.out.printf("%-20s : %-20s\n", menuInformationCustomer[i],  informationCustomer[i]);
		}
		// if (customer instanceof MemberCustomer) System.out.printf("%-20s : %-20f","Saldo Koin", ((MemberCustomer)customer).getSaldoCoin());
		System.out.println("List Kendaraan :");
		printVechicle(customer.getVehicles());
		System.out.println();
	}

	public static void printServiceList(Vehicle vehicle) {
		String formatTable = "| %-2s | %-15s | %-20s | %-15s | %-15s |%n";
		String line = "+----+-----------------+----------------------+-----------------+-----------------+-------+-----------------+%n";
		List<ItemService> listService = ItemServiceRepository.getAllItemService();
		int num = 0;

		System.out.println("List Service yang Tersedia :");
		System.out.format(line);
		System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
		System.out.format(line);
		for (ItemService itemService : listService) {
			if (itemService.getVehicleType().equalsIgnoreCase(vehicle.getVehicleType())) {
				System.out.format(formatTable, ++num, itemService.getServiceId(), itemService.getServiceName(), itemService.getVehicleType(), itemService.getPrice());
				System.out.format(line);
			}
		}
	}

	public static void printBookingMenu(Customer customer) {
		int indexId = 0;
		Vehicle vehicle = Validation.validateVehicle(customer);

		printServiceList(vehicle);
		List<ItemService> listService = Validation.validateService(vehicle, customer);
		double totalPayment = Validation.validatePayment(customer, listService);

	}

	public static void printTopUpCoinMenu(Customer customer) {
		if (customer instanceof MemberCustomer) {
			double totalTopUpCoin = Validation.validateTopUpCoin(customer);
			((MemberCustomer)customer).setSaldoCoin(((MemberCustomer)customer).getSaldoCoin() + totalTopUpCoin);
		} else System.out.println("Maaf fitur ini hanya untuk Member saja!");
	}

	public static void printBookingOrderMenu(List<BookingOrder> bookingOrders) {
		String formatTable = "| %-2s | %-17s | %-15s | %-15s | %-15s | %-15s | %-40s |%n";
		String line = "+----+-------------------+-----------------+-----------------+-----------------+-----------------+------------------------------------------+%n";

		int numIndex = 0;

		System.out.format(line);
		System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment", "List Service");
		System.out.format(line);
		for (BookingOrder bookingOrder : bookingOrders) {
			System.out.format(formatTable, ++numIndex, bookingOrder.getBookingId(), bookingOrder.getCustomer().getName(), bookingOrder.getPaymentMethod(), 0, bookingOrder.getTotalPayment(), formatServiceList(bookingOrder.getServices()));
			System.out.format(line);
		}
	}

	private static String formatServiceList(List<ItemService> Service) {
		StringBuilder stringBuilder = new StringBuilder();
		String result = "";
		for (ItemService itemService : Service) {
			stringBuilder.append(itemService.getServiceName());
		}

		return result = stringBuilder.toString();
	}
	
}
