package com.bengkel.booking.services.generate;

import com.bengkel.booking.models.Customer;

public class GenerateBookingOrderId {
    private static int indexId = 0;

    public static String generateBookingOrderId(Customer customer) {
        String custId = customer.getCustomerId().replaceAll("\\D", "");
        return String.format("Book-Cust-%03d-%s", ++indexId, custId);
    }
}
