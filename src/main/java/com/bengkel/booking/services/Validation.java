package com.bengkel.booking.services;

import java.security.Provider.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.ItemServiceRepository;
import com.bengkel.booking.repositories.list_order.ListBookingOrder;
import com.bengkel.booking.services.generate.GenerateBookingOrderId;

public class Validation {
	
	public static String validasiInput(String question, String errorMessage, String regex) {
	    Scanner input = new Scanner(System.in);
	    String result;
	    boolean isLooping = true;
	    do {
	      System.out.print(question);
	      result = input.nextLine();

	      //validasi menggunakan matches
	      if (result.matches(regex)) {
	        isLooping = false;
	      }else {
	        System.out.println(errorMessage);
	      }

	    } while (isLooping);

	    return result;
	  }
	
	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
	    int result;
	    boolean isLooping = true;
	    do {
	      result = Integer.valueOf(validasiInput(question, errorMessage, regex));
	      if (result >= min && result <= max) {
	        isLooping = false;
	      }else {
	        System.out.println("Pilihan angka " + min + " s.d " + max);
	      }
	    } while (isLooping);

	    return result;
	  }

	  public static Customer validateCustomer(List<Customer> listCustomers) {
		Scanner scanner = new Scanner(System.in);
		Customer choiceCustomer = null;
		boolean isFoundCustomerId = false;
		boolean isValidPassword = false;
	
		int faultValidateCustomer = 0;
		do {
			System.out.println("Masukkan Customer Id: ");
			String inputCustomerId = scanner.nextLine();
			choiceCustomer = listCustomers.stream()
										.filter(customer -> customer.getCustomerId().equals(inputCustomerId))
										.findFirst()
										.orElse(null);
	
			if (choiceCustomer == null) {
				System.out.println("Customer Id Tidak Ditemukan atau Salah!");
				faultValidateCustomer++;
				if (faultValidateCustomer >= 3) {
					System.out.println("Keluar dari aplikasi...");
					System.exit(0);
				}
			} else {
				isFoundCustomerId = true;
			}
		} while (!isFoundCustomerId);
	
		int faultValidatePassword = 0;
		do {
			System.out.println("Masukkan Password: ");
			String inputPassword = scanner.nextLine();
	
			if (choiceCustomer != null && choiceCustomer.getPassword().equals(inputPassword)) {
				isValidPassword = true;
			} else {
				System.out.println("Password yang Anda Masukkan Salah!");
				faultValidatePassword++;
				if (faultValidatePassword >= 3) {
					System.out.println("Terlalu banyak kesalahan. Keluar dari program.");
					System.exit(0);
				}
			}
		} while (!isValidPassword);
	
		return choiceCustomer;
	}

	public static Vehicle validateVehicle(Customer customer) {
		Scanner scanner = new Scanner(System.in);

		Vehicle vehicle = null; 
		boolean isFound = false;
		
		do {
			System.out.println("Masukan Vehicle Id: ");
			String inputVehicleId = scanner.nextLine();
			vehicle = customer.getVehicles().stream()
											.filter(vehicleId -> vehicleId.getVehiclesId().equals(inputVehicleId))
											.findFirst()
											.orElse(null);

			if (vehicle != null) isFound = true;
			else System.out.println("ID Kendaraan Tidak Ditemukan!");
		} while (!isFound);

		return vehicle;
	}

	public static List<ItemService> validateService(Vehicle vehicle, Customer customer) {
		Scanner scanner = new Scanner(System.in);

		List<ItemService> listServices = ItemServiceRepository.getAllItemService(); 
		boolean isFound = false;
		List<ItemService> getListService = new ArrayList<>();
		ItemService numService = null;

		do {
			System.out.println("Silahkan masukan Service Id: ");
			String serviceId = scanner.nextLine();
			int totalService = 0;

			numService = listServices.stream()
										.filter(vehicleType -> vehicleType.getVehicleType().equalsIgnoreCase(vehicle.getVehicleType()))
										.filter(vehicleId -> vehicleId.getServiceId().equalsIgnoreCase(serviceId))
										.findFirst()
										.orElse(null);
			
			if (numService == null) System.out.println("Service Id Tidak Ditemukan!");

			if (numService != null) {
				isFound = true;
				getListService.add(numService);
				totalService++;
				if (customer instanceof MemberCustomer && totalService < 2) {
					System.out.println("Apakah anda ingin menambahkan Service Lainnya? (Y/T)");
					boolean yesOrNot = validateYesOrNot(scanner);
					if (yesOrNot) {
						totalService++;
						continue;
					}
				}
			}
			numService = null;

		} while (!isFound);

		return getListService;
	}

	public static double validatePayment(Customer customer, List<ItemService> listItemServices) {
		Scanner scanner = new Scanner(System.in);

		double totalPayment = 0;
		double totalServicePrice = 0;
		String choice = null;
		boolean isValid = false;

		do {
			
			System.out.println("Silahkan Pilih Metode Pembayaran (Saldo Coin atau Cash)");
			choice = scanner.nextLine();
			if (choice.equalsIgnoreCase("Saldo Coin")) {
				if (customer instanceof MemberCustomer) {
					totalServicePrice = listItemServices.stream()
								.mapToDouble(ItemService::getPrice)
								.sum();
					totalPayment = listItemServices.stream()
													.mapToDouble(service -> service.getPrice() - (service.getPrice() * 0.1))
													.sum();
					((MemberCustomer)customer).setSaldoCoin(((MemberCustomer)customer).getSaldoCoin() - totalPayment);
					isValid = true;
				} else if (!(customer instanceof MemberCustomer)) System.out.println("Metode Saldo Koin Gagal, (Khusus Member)"); 
			} else if (choice.equalsIgnoreCase("Cash")) {
				totalPayment = listItemServices.stream()
								.mapToDouble(ItemService::getPrice)
								.sum();
				isValid = true;
			} else System.out.println("Input Tidak Valid!!");
		} while (!isValid);


		System.out.println("Booking Berhasil!!!");
		System.out.println("Total Harga Service		: " + ((customer instanceof MemberCustomer) ? totalServicePrice : totalPayment));
		System.out.println("Total Pembayaran		: " + totalPayment);

		BookingOrder bookingOrder = new BookingOrder(GenerateBookingOrderId.generateBookingOrderId(customer), customer, listItemServices, choice, totalServicePrice, totalPayment);
		ListBookingOrder.addBookingOrder(bookingOrder);
		return totalPayment;
	}

	public static boolean validateYesOrNot(Scanner scanner) {
		boolean isValid = true;
		boolean isYes = true;
		do {
			isValid = true;
			String yesOrNot = scanner.nextLine();
			if (yesOrNot.equalsIgnoreCase("Y")) isYes = true;
			else if (yesOrNot.equalsIgnoreCase("T")) {
				isYes = false;
				isValid = false;
			} 
			else System.out.println("Input Tidak Valid");  
		} while (isValid);
	
		return isYes;
	}

	public static double validateTopUpCoin(Customer customer) {
		Scanner scanner = new Scanner(System.in);
		boolean isValid = true;
		double totalCoin = 0;
		do {
			System.out.println("Masukan besaran Top Up: ");
			try {
				isValid = true;
				totalCoin = Double.parseDouble(scanner.nextLine());
				
			} catch (Exception e) {
				isValid = false;
				System.out.println("Input Tidak Valid");
			}
			
		} while (!isValid);

		return totalCoin;
		
	}
	
}
