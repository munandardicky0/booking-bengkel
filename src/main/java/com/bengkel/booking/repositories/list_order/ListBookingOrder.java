package com.bengkel.booking.repositories.list_order;

import java.util.ArrayList;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;

public class ListBookingOrder {
    
    private static List<BookingOrder> listBookingOrders = new ArrayList<>(); 

    public static List<BookingOrder> getAllBookingOrders() {
        return listBookingOrders;
    }

    public static void addBookingOrder(BookingOrder bookingOrder) {
        listBookingOrders.add(bookingOrder);
    }
}
